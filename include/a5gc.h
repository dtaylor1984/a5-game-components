/**
    A5 Game Components
    ------------------
    A game components library to help ease the creation of new games
    written in C++ using Allegro 5.

    (c) 2013 Desmond Taylor.
**/

#ifndef __A5_GAME_COMPONENTS_H
#define __A5_GAME_COMPONENTS_H

#define LOAD_ERROR -1

#include <string>
#include <vector>
#include <allegro5/allegro.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_ttf.h>
#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_acodec.h>

#include "eventmanager.h"
#include "timer.h"
#include "display.h"
#include "input.h"
#include "graphics.h"
#include "sound.h"
#include "font.h"

#endif // __A5_GAME_COMPONENTS_H
