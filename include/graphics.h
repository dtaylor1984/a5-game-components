#ifndef __A5_GAME_COMPONENTS_GRAPHICS_H
#define __A5_GAME_COMPONENTS_GRAPHICS_H

class Graphics
{
    struct Data
    {
        std::string name;
        ALLEGRO_BITMAP* image;
    };

    private:
        std::vector<Graphics::Data> images;
        std::vector<Graphics::Data> rotated_images;

        bool IsValidID( int id );

    public:
        Graphics();
        ~Graphics();

        // I/O Routines.
        int Load( std::string filename );
        int Load( std::string filename, std::string name );
        void Free( int id );
        void Free( std::string name );

        // Blit Routines.
        void Blit( int id, float x, float y, int flags = 0 );
        void Blit( std::string name, float x, float y, int flags = 0 );
        void BlitRegion( int id, int sx, int sy, int width, int height, float dx, float dy, int flags = 0 );
        void BlitRegion( std::string name, int sx, int sy, int width, int height, float dx, float dy, int flags = 0 );
        void BlitRotated( int id, float cx, float cy, float dx, float dy, float angle, int flags = 0 );
        void BlitRotated( std::string name, float cx, float cy, float dx, float dy, float angle, int flags = 0 );

        // Misc Routines.
        void SetMaskColor( int id, int r = 0, int g = 0, int b = 0 );
        void SetMaskColor( std::string name, int r = 0, int g = 0, int b = 0 );
        int Width( int id );
        int Width( std::string name );
        int Height( int id );
        int Height( std::string name );
        int AddBitmap( ALLEGRO_BITMAP* bitmap, std::string name = "" );
        bool SetBitmap( int id, ALLEGRO_BITMAP* bitmap );
        bool SetBitmap( std::string name, ALLEGRO_BITMAP* bitmap );
        ALLEGRO_BITMAP* GetBitmap( int id );
        ALLEGRO_BITMAP* GetBitmap( std::string name );
};

#endif // __A5_GAME_COMPONENTS_GRAPHICS_H
