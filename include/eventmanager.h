#ifndef __A5_GAME_COMPONENTS_EVENTMANAGER_H
#define __A5_GAME_COMPONENTS_EVENTMANAGER_H

class EventManager
{
    private:
        ALLEGRO_EVENT_QUEUE* queue;
        ALLEGRO_EVENT event;

    public:
        EventManager();
        ~EventManager();

        void AddSource( ALLEGRO_EVENT_SOURCE* source );
        ALLEGRO_EVENT GetNextEvent();
        bool QueueEmpty();
};

#endif // __A5_GAME_COMPONENTS_EVENTMANAGER_H
