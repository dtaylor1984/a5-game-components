#ifndef __A5_GAME_COMPONENTS_INPUT_H
#define __A5_GAME_COMPONENTS_INPUT_H

struct MouseAxis
{
    int x;
    int y;
    int z;
    int w;
    int dx;
    int dy;
    int dz;
    int dw;
    ALLEGRO_DISPLAY* display;
};

class Input
{
    private:
        bool keys[ ALLEGRO_KEY_MAX ];
        bool buttons[ 3 ];
        MouseAxis mouseAxis;

    public:
        Input( EventManager* eventManager );
        ~Input();

        void Update( ALLEGRO_EVENT event );
        bool KeyDown( int keyCode );
        bool KeyUp( int keyCode );
        void HideMouse( ALLEGRO_DISPLAY* display );
        void ShowMouse( ALLEGRO_DISPLAY* display );
        bool MouseDown( int button );
        bool MouseUp( int button );
        MouseAxis GetMouseAxis();
};

#endif // __A5_GAME_COMPONENTS_INPUT_H
