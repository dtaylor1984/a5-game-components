#ifndef __A5_GAME_COMPONENTS_DISPLAY_H
#define __A5_GAME_COMPONENTS_DISPLAY_H

class Display
{
    private:
        ALLEGRO_DISPLAY* display;

    public:
        Display( int width, int height, int flags = 0 );
        ~Display();

        void SetTitle( std::string title );
        void SetTarget();
        void Clear( int r = 0, int g = 0, int b = 0 );
        void Flip();
        ALLEGRO_EVENT_SOURCE* GetEventSource();
        ALLEGRO_DISPLAY* GetDisplay();
};

#endif // __A5_GAME_COMPONENTS_DISPLAY_H
