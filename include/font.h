#ifndef __A5_GAME_COMPONENTS_FONT_H
#define __A5_GAME_COMPONENTS_FONT_H

class Font
{
    struct Data
    {
        std::string name;
        ALLEGRO_FONT* font;
    };

    private:
        std::vector<Font::Data> fonts;

        bool IsValidID( int id );

    public:
        Font();
        ~Font();

        // I/O Routines.
        int Load( std::string filename, int size, int flags = ALLEGRO_TTF_MONOCHROME );
        int Load( std::string filename, std::string name, int size, int flags = ALLEGRO_TTF_MONOCHROME );
        void Free( int id );
        void Free( std::string name );

        // Blit Routines.
        void Blit( int id, std::string text, float x, float y, ALLEGRO_COLOR color, int flags = 0 );
        void Blit( std::string name, std::string text, float x, float y, ALLEGRO_COLOR color, int flags = 0 );
};

#endif // __A5_GAME_COMPONENTS_FONT_H
