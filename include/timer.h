#ifndef __A5_GAME_COMPONENTS_TIMER_H
#define __A5_GAME_COMPONENTS_TIMER_H

class Timer
{
    private:
        ALLEGRO_TIMER* timer;

    public:
        Timer( double timeout_seconds = 0.0333333333333333 ); // Default 30 FPS. (1.0 / 30)
        ~Timer();

        void SetTimeout( double timeout_seconds );
        void Start();
        void Stop();
        ALLEGRO_TIMER* GetTimer();
        ALLEGRO_EVENT_SOURCE* GetEventSource();
};

#endif // __A5_GAME_COMPONENTS_TIMER_H
