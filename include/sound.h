#ifndef __A5_GAME_COMPONENTS_SOUND_H
#define __A5_GAME_COMPONENTS_SOUND_H

class Sound
{
    struct Data
    {
        std::string name;
        ALLEGRO_SAMPLE* sample;
        ALLEGRO_SAMPLE_ID sampleID;
    };

    private:
        std::vector<Sound::Data> samples;

        bool IsValidID( int id );

    public:
        Sound();
        ~Sound();

        // I/O Routines.
        int Load( std::string filename );
        int Load( std::string filename, std::string name );
        void Free( int id );
        void Free( std::string name );

        // Play/Stop Routines.
        void Play( int id, ALLEGRO_PLAYMODE loop = ALLEGRO_PLAYMODE_ONCE, float gain = 1.0, float pan = 0.0, float speed = 1.0 );
        void Play( std::string name, ALLEGRO_PLAYMODE loop = ALLEGRO_PLAYMODE_ONCE, float gain = 1.0, float pan = 0.0, float speed = 1.0 );
        void Stop( int id );
        void Stop( std::string name );
};

#endif // __A5_GAME_COMPONENTS_SOUND_H
