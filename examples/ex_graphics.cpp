#include <a5gc.h>
#include <stdio.h>

int main()
{
    EventManager* eventManager = new EventManager();
    Display* display = new Display( 640, 480, ALLEGRO_WINDOWED );
    Font* font = new Font();
    Graphics* gfx = new Graphics;

    display->SetTitle( "A5 Game Components - Graphics Example" );
    eventManager->AddSource( display->GetEventSource() );

    if ( font->Load( "data/verdana.ttf", "smallFont", 12 ) == LOAD_ERROR )
        fprintf( stderr, "Failed to load font \"data/verdana.ttf\"\n" );

    if ( gfx->Load( "data/frog.jpg", "frog" ) == LOAD_ERROR )
        fprintf( stderr, "Failed to load image \"data/frog.jpg\"\n" );

    bool gameRunning = true;
    while ( gameRunning )
    {
        ALLEGRO_EVENT event = eventManager->GetNextEvent();
        switch ( event.type )
        {
            case ALLEGRO_EVENT_DISPLAY_CLOSE:
                gameRunning = false;
            break;
        }

        display->Clear();

        gfx->Blit( "frog", 0, 0 );
        font->Blit( "smallFont", "A5 Game Components!", 10, 10, al_map_rgb( 255, 255, 255 ) );

        display->Flip();

        al_rest( 0.001 );
    }

    delete eventManager;
    delete display;
    delete font;
    delete gfx;

    return 0;
}
