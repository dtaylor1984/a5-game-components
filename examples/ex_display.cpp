#include <a5gc.h>

int main()
{
    Display* display = new Display( 640, 480, ALLEGRO_WINDOWED );

    display->SetTitle( "A5 Game Components - Display Example" );

    display->Clear();

    display->Flip();

    al_rest( 5 ); // Wait 5 seconds so we can see our window.

    delete display;

    return 0;
}
