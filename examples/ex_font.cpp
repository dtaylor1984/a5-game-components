#include <a5gc.h>
#include <stdio.h>

int main()
{
    EventManager* eventManager = new EventManager();
    Display* display = new Display( 640, 480, ALLEGRO_WINDOWED );
    Font* font = new Font();

    display->SetTitle( "A5 Game Components - Font Example" );
    eventManager->AddSource( display->GetEventSource() );

    if ( font->Load( "data/verdana.ttf", "smallFont", 12 ) == LOAD_ERROR )
        fprintf( stderr, "Failed to load font \"data/verdana.ttf\"\n" );

    bool gameRunning = true;
    while ( gameRunning )
    {
        ALLEGRO_EVENT event = eventManager->GetNextEvent();
        switch ( event.type )
        {
            case ALLEGRO_EVENT_DISPLAY_CLOSE:
                gameRunning = false;
            break;
        }

        display->Clear();

        font->Blit( "smallFont", "A5 Game Components!", 10, 10, al_map_rgb( 255, 255, 255 ) );

        display->Flip();

        al_rest( 0.001 );
    }

    delete eventManager;
    delete display;
    delete font;

    return 0;
}
