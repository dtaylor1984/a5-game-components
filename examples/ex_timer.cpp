#include <a5gc.h>
#include <stdio.h>

int main()
{
    EventManager* eventManager = new EventManager();
    Display* display = new Display( 640, 480, ALLEGRO_WINDOWED );
    Font* font = new Font();
    Graphics* gfx = new Graphics;
    Input* input = new Input( eventManager );
    Sound* sound = new Sound();
    Timer* fpsTimer = new Timer( 1.0 / 60 ); // 60 FPS

    display->SetTitle( "A5 Game Components - Timer Example" );

    eventManager->AddSource( display->GetEventSource() );
    eventManager->AddSource( fpsTimer->GetEventSource() );

    fpsTimer->Start();

    if ( font->Load( "data/verdana.ttf", "smallFont", 12 ) == LOAD_ERROR )
        fprintf( stderr, "Failed to load font \"data/verdana.ttf\"\n" );

    if ( gfx->Load( "data/frog.jpg", "frog" ) == LOAD_ERROR )
        fprintf( stderr, "Failed to load image \"data/frog.jpg\"\n" );

    if ( sound->Load( "data/music.ogg", "music" ) == LOAD_ERROR )
        fprintf( stderr, "Failed to load sound \"data/music.ogg\"\n" );

    sound->Play( "music" );

    bool gameRunning = true;
    bool redraw = true;
    while ( gameRunning )
    {
        ALLEGRO_EVENT event = eventManager->GetNextEvent();

        input->Update( event );

        switch ( event.type )
        {
            case ALLEGRO_EVENT_DISPLAY_CLOSE:
                gameRunning = false;
            break;

            case ALLEGRO_EVENT_TIMER:
                if ( event.any.source == fpsTimer->GetEventSource() )
                    redraw = true;
            break;
        }

        if ( input->KeyDown( ALLEGRO_KEY_ESCAPE ) )
            gameRunning = false;

        if ( redraw )
        {
            redraw = false;

            display->Clear();

            gfx->Blit( "frog", 0, 0 );
            font->Blit( "smallFont", "A5 Game Components!", 10, 10, al_map_rgb( 255, 255, 255 ) );

            display->Flip();
        }

        al_rest( 0.001 );
    }

    delete eventManager;
    delete display;
    delete font;
    delete gfx;
    delete input;
    delete sound;
    delete fpsTimer;

    return 0;
}
