#include <a5gc.h>

int main()
{
    EventManager* eventManager = new EventManager();
    Display* display = new Display( 640, 480, ALLEGRO_WINDOWED );

    display->SetTitle( "A5 Game Components - Events Example" );
    eventManager->AddSource( display->GetEventSource() );

    bool gameRunning = true;
    while ( gameRunning )
    {
        ALLEGRO_EVENT event = eventManager->GetNextEvent();
        switch ( event.type )
        {
            case ALLEGRO_EVENT_DISPLAY_CLOSE:
                gameRunning = false;
            break;
        }

        display->Clear();

        display->Flip();

        al_rest( 0.001 );
    }

    delete eventManager;
    delete display;

    return 0;
}
