A5 Game Components is a static library made for personal use that helps to make game creation much quicker. Feel free to use this library in your own work.

## The core components are (A-Z):
  - Display
  - EventManager
  - Font
  - Graphics
  - Input
  - Sound
  - Timer

A5 Game Components uses OOP (Object Oriented Programming) so can only be used with a C++ compiler.