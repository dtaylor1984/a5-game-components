#include "a5gc.h"

Display::Display( int width, int height, int flags )
{
    al_init();

    if ( flags != 0 )
        al_set_new_display_flags( flags );

    this->display = al_create_display( width, height );
}

Display::~Display()
{
    al_destroy_display( this->display );
}

void Display::SetTitle( std::string title )
{
    al_set_window_title( this->display, title.c_str() );
}

void Display::SetTarget()
{
    al_set_target_bitmap( al_get_backbuffer( this->display ) );
}

void Display::Clear( int r, int g, int b )
{
    this->SetTarget();
    al_clear_to_color( al_map_rgb( r, g, b ) );
}

void Display::Flip()
{
    al_flip_display();
}

ALLEGRO_EVENT_SOURCE* Display::GetEventSource()
{
    return al_get_display_event_source( this->display );
}

ALLEGRO_DISPLAY* Display::GetDisplay()
{
    return this->display;
}
