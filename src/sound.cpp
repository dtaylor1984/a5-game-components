#include "a5gc.h"

Sound::Sound()
{
    al_install_audio();

    al_init_acodec_addon();

    al_reserve_samples( 32 );
}

Sound::~Sound()
{
    al_stop_samples();

    for ( unsigned int i=0; i<this->samples.size(); i++ )
    {
        if ( this->samples[i].sample )
            al_destroy_sample( this->samples[i].sample );
    }

    al_uninstall_audio();
}

int Sound::Load( std::string filename )
{
    ALLEGRO_SAMPLE* tmpSample = al_load_sample( filename.c_str() );
    if ( !tmpSample )
        return LOAD_ERROR;

    for ( unsigned int i=0; i<this->samples.size(); i++ )
    {
        if ( !this->samples[i].sample )
        {
            this->samples[i].name = "";
            this->samples[i].sample = tmpSample;
            return i;
        }
    }

    Sound::Data data;
    data.sample = tmpSample;
    this->samples.push_back( data );

    return this->samples.size() - 1;
}

int Sound::Load( std::string filename, std::string name )
{
    ALLEGRO_SAMPLE* tmpSample = al_load_sample( filename.c_str() );
    if ( !tmpSample )
        return LOAD_ERROR;

    for ( unsigned int i=0; i<this->samples.size(); i++ )
    {
        if ( this->samples[i].name == name )
            return LOAD_ERROR;

        if ( !this->samples[i].sample )
        {
            this->samples[i].name = name;
            this->samples[i].sample = tmpSample;
            return i;
        }
    }

    Sound::Data data;
    data.name = name;
    data.sample = tmpSample;
    this->samples.push_back( data );

    return this->samples.size() - 1;
}

void Sound::Free( int id )
{
    if ( this->IsValidID( id ) )
    {
        this->Stop( id );
        al_destroy_sample( this->samples[id].sample );
        this->samples[id].name = "";
        this->samples[id].sample = NULL;
    }
}

void Sound::Free( std::string name )
{
    for ( unsigned int i=0; i<this->samples.size(); i++ )
    {
        if ( this->samples[i].name == name )
        {
            this->Stop( i );
            al_destroy_sample( this->samples[i].sample );
            this->samples[i].name = "";
            this->samples[i].sample = NULL;
        }
    }
}

bool Sound::IsValidID( int id )
{
    if ( id > (signed)this->samples.size() || id < 0 )
        return false;

    if ( !this->samples[id].sample )
        return false;

    return true;
}

void Sound::Play( int id, ALLEGRO_PLAYMODE loop, float gain, float pan, float speed )
{
    if ( this->IsValidID( id ) )
        al_play_sample( this->samples[id].sample, gain, pan, speed, loop, &this->samples[id].sampleID );
}

void Sound::Play( std::string name, ALLEGRO_PLAYMODE loop, float gain, float pan, float speed )
{
    for ( unsigned int i=0; i<this->samples.size(); i++ )
    {
        if ( this->samples[i].name == name )
            this->Play( i, loop, gain, pan, speed );
    }
}

void Sound::Stop( int id )
{
    if ( this->IsValidID( id ) )
        al_stop_sample( &this->samples[id].sampleID );
}

void Sound::Stop( std::string name )
{
    for ( unsigned int i=0; i<this->samples.size(); i++ )
    {
        if ( this->samples[i].name == name )
            this->Stop( i );
    }
}
