#include "a5gc.h"

Timer::Timer( double timeout_seconds )
{
    al_init();

    this->timer = al_create_timer( timeout_seconds );
}

Timer::~Timer()
{
    this->Stop();

    al_destroy_timer( this->timer );
}

void Timer::SetTimeout( double timeout_seconds )
{
    al_set_timer_speed( this->timer, timeout_seconds );
}

void Timer::Start()
{
    al_start_timer( this->timer );
}

void Timer::Stop()
{
    al_stop_timer( this->timer );
}

ALLEGRO_TIMER* Timer::GetTimer()
{
    return this->timer;
}

ALLEGRO_EVENT_SOURCE* Timer::GetEventSource()
{
    return al_get_timer_event_source( this->timer );
}
