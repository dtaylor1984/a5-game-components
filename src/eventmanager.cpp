#include "a5gc.h"

EventManager::EventManager()
{
    al_init();

    this->queue = al_create_event_queue();
}

EventManager::~EventManager()
{
    al_destroy_event_queue( this->queue );
}

void EventManager::AddSource( ALLEGRO_EVENT_SOURCE* source )
{
    al_register_event_source( this->queue, source );
}

ALLEGRO_EVENT EventManager::GetNextEvent()
{
    if ( al_get_next_event( this->queue, &this->event ) )
    {
        return this->event;
    }

    ALLEGRO_EVENT nullEvent;
    return nullEvent;
}

bool EventManager::QueueEmpty()
{
    return al_event_queue_is_empty( this->queue );
}
