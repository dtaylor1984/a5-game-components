#include "a5gc.h"

Input::Input( EventManager* eventManager )
{
    al_init();

    al_install_keyboard();
    al_install_mouse();

    eventManager->AddSource( al_get_keyboard_event_source() );
    eventManager->AddSource( al_get_mouse_event_source() );

    memset( this->keys, false, sizeof( this->keys ) );
    memset( this->buttons, false, sizeof( this->buttons ) );
}

Input::~Input()
{
    al_uninstall_keyboard();
    al_uninstall_mouse();
}

void Input::Update( ALLEGRO_EVENT event )
{
    switch ( event.type )
    {
        case ALLEGRO_EVENT_KEY_DOWN:
            this->keys[event.keyboard.keycode] = true;
        break;

        case ALLEGRO_EVENT_KEY_UP:
            this->keys[event.keyboard.keycode] = false;
        break;

        case ALLEGRO_EVENT_MOUSE_AXES:
            MouseAxis ma;
            ma.x = event.mouse.x;
            ma.y = event.mouse.y;
            ma.z = event.mouse.z;
            ma.w = event.mouse.w;
            ma.dx = event.mouse.dx;
            ma.dy = event.mouse.dy;
            ma.dz = event.mouse.dz;
            ma.dw = event.mouse.dw;
            ma.display = event.mouse.display;
            this->mouseAxis = ma;
        break;

        case ALLEGRO_EVENT_MOUSE_BUTTON_DOWN:
            this->buttons[event.mouse.button] = true;
        break;

        case ALLEGRO_EVENT_MOUSE_BUTTON_UP:
            this->buttons[event.mouse.button] = false;
        break;
    }
}

bool Input::KeyDown( int keyCode )
{
    return this->keys[keyCode];
}

bool Input::KeyUp( int keyCode )
{
    return !this->keys[keyCode];
}

void Input::HideMouse( ALLEGRO_DISPLAY* display )
{
        al_hide_mouse_cursor( display );
}

void Input::ShowMouse( ALLEGRO_DISPLAY* display )
{
        al_show_mouse_cursor( display );
}

bool Input::MouseDown( int button )
{
    return this->buttons[button];
}

bool Input::MouseUp( int button )
{
    return !this->buttons[button];
}

MouseAxis Input::GetMouseAxis()
{
    return this->mouseAxis;
}
