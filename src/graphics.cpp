#include "a5gc.h"

Graphics::Graphics()
{
    al_init_image_addon();
}

Graphics::~Graphics()
{
    for ( unsigned int i=0; i<this->images.size(); i++ )
    {
        if ( this->images[i].image )
            al_destroy_bitmap( this->images[i].image );
    }

    for ( unsigned int i=0; i<this->rotated_images.size(); i++ )
    {
        if ( this->rotated_images[i].image )
            al_destroy_bitmap( this->rotated_images[i].image );
    }

    al_shutdown_image_addon();
}

int Graphics::Load( std::string filename )
{
    ALLEGRO_BITMAP* tmpBmp = al_load_bitmap( filename.c_str() );
    if ( !tmpBmp )
        return LOAD_ERROR;

    for ( unsigned int i=0; i<this->images.size(); i++ )
    {
        if ( !this->images[i].image )
        {
            this->images[i].name = "";
            this->images[i].image = tmpBmp;
            return i;
        }
    }

    Graphics::Data data;
    data.image = tmpBmp;
    this->images.push_back( data );

    return this->images.size() - 1;
}

int Graphics::Load( std::string filename, std::string name )
{
    ALLEGRO_BITMAP* tmpBmp = al_load_bitmap( filename.c_str() );
    if ( !tmpBmp )
        return LOAD_ERROR;

    for ( unsigned int i=0; i<this->images.size(); i++ )
    {
        if ( this->images[i].name == name )
            return LOAD_ERROR;

        if ( !this->images[i].image )
        {
            this->images[i].name = name;
            this->images[i].image = tmpBmp;
            return i;
        }
    }

    Graphics::Data data;
    data.name = name;
    data.image = tmpBmp;
    this->images.push_back( data );

    return this->images.size() - 1;
}

bool Graphics::IsValidID( int id )
{
    if ( id > (signed)this->images.size() || id < 0 )
        return false;

    if ( !this->images[id].image )
        return false;

    return true;
}

void Graphics::Free( int id )
{
    if ( this->IsValidID( id ) )
    {
        al_destroy_bitmap( this->images[id].image );
        this->images[id].name = "";
        this->images[id].image = NULL;
    }
}

void Graphics::Free( std::string name )
{
    for ( unsigned int i=0; i<this->images.size(); i++ )
    {
        if ( this->images[i].name == name )
        {
            al_destroy_bitmap( this->images[i].image );
            this->images[i].name = "";
            this->images[i].image = NULL;
        }
    }
}

void Graphics::Blit( int id, float x, float y, int flags )
{
    if ( this->IsValidID( id ) )
    {
        al_draw_bitmap( this->images[id].image, x, y, flags );
    }
}

void Graphics::Blit( std::string name, float x, float y, int flags )
{
    for ( unsigned int i=0; i<this->images.size(); i++ )
    {
        if ( this->images[i].name == name )
            this->Blit( i, x, y, flags );
    }
}

void Graphics::BlitRegion( int id, int sx, int sy, int width, int height, float dx, float dy, int flags )
{
    if ( this->IsValidID( id ) )
    {
        al_draw_bitmap_region( this->images[id].image, sx, sy, width, height, dx, dy, flags );
    }
}

void Graphics::BlitRegion( std::string name, int sx, int sy, int width, int height, float dx, float dy, int flags )
{
    for ( unsigned int i=0; i<this->images.size(); i++ )
    {
        if ( this->images[i].name == name )
            this->BlitRegion( i, sx, sy, width, height, dx, dy, flags );
    }
}

void Graphics::BlitRotated( int id, float cx, float cy, float dx, float dy, float angle, int flags )
{
    if ( this->IsValidID( id ) )
    {
        al_draw_rotated_bitmap( this->images[id].image, cx, cy, dx, dy, angle, flags );
    }
}

void Graphics::BlitRotated( std::string name, float cx, float cy, float dx, float dy, float angle, int flags )
{
    for ( unsigned int i=0; i<this->images.size(); i++ )
    {
        if ( this->images[i].name == name )
            this->BlitRotated( i, cx, cy, dx, dy, angle, flags );
    }
}

void Graphics::SetMaskColor( int id, int r, int g, int b )
{
    if ( this->IsValidID( id ) )
        al_convert_mask_to_alpha( this->images[id].image, al_map_rgb( r, g, b ) );
}

void Graphics::SetMaskColor( std::string name, int r, int g, int b )
{
    for ( unsigned int i=0; i<this->images.size(); i++ )
    {
        if ( this->images[i].name == name )
            al_convert_mask_to_alpha( this->images[i].image, al_map_rgb( r, g, b ) );
    }
}

int Graphics::Width( int id )
{
    if ( this->IsValidID( id ) )
        return al_get_bitmap_width( this->images[id].image );

    return 0;
}

int Graphics::Width( std::string name )
{
    for ( unsigned int i=0; i<this->images.size(); i++ )
    {
        if ( this->images[i].name == name )
            return al_get_bitmap_width( this->images[i].image );
    }

    return 0;
}

int Graphics::Height( int id )
{
    if ( this->IsValidID( id ) )
        return al_get_bitmap_height( this->images[id].image );

    return 0;
}

int Graphics::Height( std::string name )
{
    for ( unsigned int i=0; i<this->images.size(); i++ )
    {
        if ( this->images[i].name == name )
            return al_get_bitmap_height( this->images[i].image );
    }

    return 0;
}

int Graphics::AddBitmap( ALLEGRO_BITMAP* bitmap, std::string name )
{
    if ( !bitmap )
        return LOAD_ERROR;

    for ( unsigned int i=0; i<this->images.size(); i++ )
    {
        if ( this->images[i].name == name )
            return LOAD_ERROR;

        if ( !this->images[i].image )
        {
            this->images[i].name = name;
            this->images[i].image = bitmap;
            return i;
        }
    }

    Graphics::Data data;
    data.name = name;
    data.image = bitmap;
    this->images.push_back( data );

    return this->images.size() - 1;
}

bool Graphics::SetBitmap( int id, ALLEGRO_BITMAP* bitmap )
{
    if ( this->IsValidID( id ) )
    {
        this->images[id].image = bitmap;
        return true;
    }

    return false;
}

bool Graphics::SetBitmap( std::string name, ALLEGRO_BITMAP* bitmap )
{
    for ( unsigned int i=0; i<this->images.size(); i++ )
    {
        if ( this->images[i].name == name )
            return this->SetBitmap( i, bitmap );
    }

    return false;
}

ALLEGRO_BITMAP* Graphics::GetBitmap( int id )
{
    if ( this->IsValidID( id ) )
        return this->images[id].image;

    return NULL;
}

ALLEGRO_BITMAP* Graphics::GetBitmap( std::string name )
{
    for ( unsigned int i=0; i<this->images.size(); i++ )
    {
        if ( this->images[i].name == name )
            return this->GetBitmap( i );
    }

    return NULL;
}
