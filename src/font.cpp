#include "a5gc.h"

Font::Font()
{
    al_init_font_addon();
    al_init_ttf_addon();
}

Font::~Font()
{
    for ( unsigned int i=0; i<this->fonts.size(); i++ )
    {
        if ( this->fonts[i].font )
            al_destroy_font( this->fonts[i].font );
    }

    al_shutdown_font_addon();
    al_shutdown_ttf_addon();
}

int Font::Load( std::string filename, int size, int flags )
{
    ALLEGRO_FONT* tmpFont = al_load_font( filename.c_str(), size, flags );
    if ( !tmpFont )
        return LOAD_ERROR;

    for ( unsigned int i=0; i<this->fonts.size(); i++ )
    {
        if ( !this->fonts[i].font )
        {
            this->fonts[i].name = "";
            this->fonts[i].font = tmpFont;
            return i;
        }
    }

    Font::Data data;
    data.font = tmpFont;
    this->fonts.push_back( data );

    return this->fonts.size() - 1;
}

int Font::Load( std::string filename, std::string name, int size, int flags )
{
    ALLEGRO_FONT* tmpFont = al_load_font( filename.c_str(), size, flags );
    if ( !tmpFont )
        return LOAD_ERROR;

    for ( unsigned int i=0; i<this->fonts.size(); i++ )
    {
        if ( this->fonts[i].name == name )
            return LOAD_ERROR;

        if ( !this->fonts[i].font )
        {
            this->fonts[i].name = name;
            this->fonts[i].font = tmpFont;
            return i;
        }
    }

    Font::Data data;
    data.name = name;
    data.font = tmpFont;
    this->fonts.push_back( data );

    return this->fonts.size() - 1;
}

bool Font::IsValidID( int id )
{
    if ( id > (signed)this->fonts.size() || id < 0 )
        return false;

    if ( !this->fonts[id].font )
        return false;

    return true;
}

void Font::Free( int id )
{
    if ( this->IsValidID( id ) )
    {
        al_destroy_font( this->fonts[id].font );
        this->fonts[id].name = "";
        this->fonts[id].font = NULL;
    }
}

void Font::Free( std::string name )
{
    for ( unsigned int i=0; i<this->fonts.size(); i++ )
    {
        if ( this->fonts[i].name == name )
        {
            al_destroy_font( this->fonts[i].font );
            this->fonts[i].name = "";
            this->fonts[i].font = NULL;
        }
    }
}

void Font::Blit( int id, std::string text, float x, float y, ALLEGRO_COLOR color, int flags )
{
    if ( this->IsValidID( id ) )
        al_draw_text( this->fonts[id].font, color, x, y, flags, text.c_str() );
}

void Font::Blit( std::string name, std::string text, float x, float y, ALLEGRO_COLOR color, int flags )
{
    for ( unsigned int i=0; i<this->fonts.size(); i++ )
    {
        if ( this->fonts[i].name == name )
            this->Blit( i, text, x, y, color, flags );
    }
}
